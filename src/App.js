import './App.css';
import Home from './_components/home';

function App() {
  return <Home />;
}

export default App;
