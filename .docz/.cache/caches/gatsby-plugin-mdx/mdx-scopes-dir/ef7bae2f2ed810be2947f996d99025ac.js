import { Playground, Props } from 'docz';
import { Alert } from "..\\..\\..\\..\\..\\src\\_components\\Alert";
import * as React from 'react';
export default {
  Playground,
  Props,
  Alert,
  React
};