const { mergeWith } = require('docz-utils')
const fs = require('fs-extra')

let custom = {}
const hasGatsbyConfig = fs.existsSync('./gatsby-config.custom.js')

if (hasGatsbyConfig) {
  try {
    custom = require('./gatsby-config.custom')
  } catch (err) {
    console.error(
      `Failed to load your gatsby-config.js file : `,
      JSON.stringify(err),
    )
  }
}

const config = {
  pathPrefix: '/',

  siteMetadata: {
    title: 'Mspr',
    description: 'My awesome app using docz',
  },
  plugins: [
    {
      resolve: 'gatsby-theme-docz',
      options: {
        themeConfig: {},
        src: './',
        gatsbyRoot: null,
        themesDir: 'src',
        mdxExtensions: ['.md', '.mdx'],
        docgenConfig: {},
        menu: [],
        mdPlugins: [],
        hastPlugins: [],
        ignore: [],
        typescript: false,
        ts: false,
        propsParser: true,
        'props-parser': true,
        debug: false,
        native: false,
        openBrowser: null,
        o: null,
        open: null,
        'open-browser': null,
        root: 'D:\\EPSI\\i1\\MSPR_IntegrationContinue\\sandbox\\.docz',
        base: '/',
        source: './',
        'gatsby-root': null,
        files: '**/*.{md,markdown,mdx}',
        public: '/public',
        dest: '.docz/dist',
        d: '.docz/dist',
        editBranch: 'master',
        eb: 'master',
        'edit-branch': 'master',
        config: '',
        title: 'Mspr',
        description: 'My awesome app using docz',
        host: 'localhost',
        port: 3000,
        p: 3000,
        separator: '-',
        paths: {
          root: 'D:\\EPSI\\i1\\MSPR_IntegrationContinue\\sandbox',
          templates:
            'D:\\EPSI\\i1\\MSPR_IntegrationContinue\\sandbox\\node_modules\\docz-core\\dist\\templates',
          docz: 'D:\\EPSI\\i1\\MSPR_IntegrationContinue\\sandbox\\.docz',
          cache:
            'D:\\EPSI\\i1\\MSPR_IntegrationContinue\\sandbox\\.docz\\.cache',
          app: 'D:\\EPSI\\i1\\MSPR_IntegrationContinue\\sandbox\\.docz\\app',
          appPackageJson:
            'D:\\EPSI\\i1\\MSPR_IntegrationContinue\\sandbox\\package.json',
          appTsConfig:
            'D:\\EPSI\\i1\\MSPR_IntegrationContinue\\sandbox\\tsconfig.json',
          gatsbyConfig:
            'D:\\EPSI\\i1\\MSPR_IntegrationContinue\\sandbox\\gatsby-config.js',
          gatsbyBrowser:
            'D:\\EPSI\\i1\\MSPR_IntegrationContinue\\sandbox\\gatsby-browser.js',
          gatsbyNode:
            'D:\\EPSI\\i1\\MSPR_IntegrationContinue\\sandbox\\gatsby-node.js',
          gatsbySSR:
            'D:\\EPSI\\i1\\MSPR_IntegrationContinue\\sandbox\\gatsby-ssr.js',
          importsJs:
            'D:\\EPSI\\i1\\MSPR_IntegrationContinue\\sandbox\\.docz\\app\\imports.js',
          rootJs:
            'D:\\EPSI\\i1\\MSPR_IntegrationContinue\\sandbox\\.docz\\app\\root.jsx',
          indexJs:
            'D:\\EPSI\\i1\\MSPR_IntegrationContinue\\sandbox\\.docz\\app\\index.jsx',
          indexHtml:
            'D:\\EPSI\\i1\\MSPR_IntegrationContinue\\sandbox\\.docz\\app\\index.html',
          db:
            'D:\\EPSI\\i1\\MSPR_IntegrationContinue\\sandbox\\.docz\\app\\db.json',
        },
      },
    },
  ],
}

const merge = mergeWith((objValue, srcValue) => {
  if (Array.isArray(objValue)) {
    return objValue.concat(srcValue)
  }
})

module.exports = merge(config, custom)
